#!/usr/bin/env python3
# -*- coding: utf-8 -*-


""" Optimise the gamma parameters """


from functools import partial
import random
import numpy as np
import scipy.stats as st
from scipy.stats import gamma
import scipy.optimize as optim
### NOTE: scipy uses the gamma definition where mean = a * scale.
#         I will use the standard definition in phylogenetics with parameters 
#         alpha and beta, such that mean = alpha/beta

# Take a look as gamma.expect(func, args)

# We need to warn the user about underflows and overflows (very small likehoods and such)
#np.seterr(under='warn', over='warn')
import logging
logger = logging.getLogger(__name__)


def joint_likelihood_gamma(param_value, cond_likelihood, shape, loc=0, beta=1):
    return (cond_likelihood(param_value, shape, loc, 1/beta)
            * gamma.pdf(param_value, shape, loc, 1/beta))


def joint_loglikelihood_gamma(param_value, cond_loglikelihood, shape, loc=0, beta=1):
    return (cond_loglikelihood(param_value, shape, loc, 1/beta)
            + gamma.logpdf(param_value, shape, loc, 1/beta))


def discrete_gamma(k, shape, loc=0, beta=1):
    """Return k+1 bins, k heights."""
    # See Ziheng Yang 1994, Maximum Likelihood Phylogenetic Estimation from DNA Sequences with Variable Rates over Sites: Approximate Methods.
    # Note that:
    #   r ~ Gamma(a,b/c) <=> c*r ~ Gamma(a, b)
    #   Gamma(a, 1/2) = Chi²(2*a)
    # But anyway, we can directly use scipy percentile points with `gamma.ppf`
    scale = 1./beta
    bins_k = gamma.ppf(np.linspace(0, 1, k+1), shape, loc, scale)

    means_k = [gamma.expect(args=(shape,), loc=loc, scale=scale, lb=lb, ub=ub, conditional=True)
                for lb, ub in zip(bins_k[:-1], bins_k[1:])]

    return bins_k, means_k


def test_discrete_gamma():
    # Example from Z. Yang 1994
    bins4, means4 = discrete_gamma(4, .5, beta=.5)
    assert np.allclose(bins4[1:-1], [0.1015, 0.4549, 1.3233], atol=1e-4)
    assert np.allclose(means4, [0.0334, 0.2519, 0.8203, 2.8944], atol=1e-4)


def test_discrete_gamma_pk(k, shape, loc=0, beta=1):
    bins_k, means_k = discrete_gamma(k, shape, loc, beta)
    
    scale = 1./beta
    bin_probas = [gamma.cdf(ub, shape, loc, scale) - gamma.cdf(lb, shape, loc, scale)
                  for lb, ub in zip(bins_k[:-1], bins_k[1:])]
    assert np.allclose(bin_probas, 1./k), '%g VS %s' %(1./k, bin_probas)


def marginal_likelihood_discrete_distrib(cond_like, bins, means):
    pk = 1./means.shape[0]
    #if cond_like is vectorised:
    return (cond_like(means) * pk).sum()
    #else
    #return np.sum([cond_like(m) * pk for m in means])


def posterior_proba_belonging():
    """Empirical Bayes posterior probability of data to be in rate category k."""


def conditional_likelihood_normal_with_gamma_p(x, shape, loc, beta):
    # This is the conditional expectation of p(x|p) (binomial), on the gamma parameters.
    # Can use a vector of shapes if x is a scalar.
    scale = 1. / beta
    #proba_x = lambda p: st.norm.pdf(x, loc=p).prod()  # Function of p where p ~ Gamma(alpha, beta)
    logproba_x = lambda p: st.norm.logpdf(x, loc=p).sum()  # Function of p where p ~ Gamma(alpha, beta)
    # proba_x is too small
    return gamma.expect(proba_x, args=(shape,), loc=loc, scale=scale)


# Specific case for illustration
def conditional_likelihood_normal_with_gamma_p_discrete(x, shape=2, loc=0, beta=2, k=4):
    logproba_x = lambda p: st.norm.logpdf(x, loc=p, scale=3).sum()  # Function of p where p ~ Gamma(alpha, beta)
    bins, means = discrete_gamma(k, shape, loc, beta)
    pk = 1. / k
    # Log of the approximate expectation
    return np.sum(proba_x(p)*pk for p in means)


def simul_normal_with_gamma_p(shape=2, loc=0, beta=2, n=1000):
    """A normal variable X with mean m and std 1, where m is gamma distributed."""
    z = gamma.rvs(shape, loc, 1./beta, size=n)
    return st.norm.rvs(loc=z, scale=3)


def conditional_expect_loglikelihood_with_gamma_p_discrete(x, logproba, shape=2, loc=0, k=4):
    """ Let Y be the hidden values parametrising logproba.

    E_{Y|gamma params} ( logP(X,Y|gamma params) )"""
    bins, means = discrete_gamma(k, shape, loc, beta)
    pk = 1. / k
    return np.sum(logproba(y)*pk for y in means)


def optim_hidden_gamma_params(proba, k=4):
    """Optimizes the likelihood of the gamma params (a,b): P(X|a,b)
    
    With P(X|a,b) =~ sum( P(X|m) * P(m|a,b) )
    knowing that m ~ Gamma(a,b) and that P(X|m) is a function of m given by arg `proba`
    
    """

    pk = 1. / k
    def target_neglikelihood(shape, loc, beta):
        bins, means = discrete_gamma(k, shape, loc, beta)
        return -np.sum([proba(m)*pk for m in means], dtype=np.longdouble)

    def target_negloglikelihood(shape, loc, beta):
        bins, means = discrete_gamma(k, shape, loc, beta)
        logprobs = np.array([logproba(m) for m in means], dtype=np.longdouble)
        # we'll factor out the smallest power, for summing.
        logfactor = max(logprobs)
        rlogprobs = logprobs - logfactor
        # Now compute the exponentials and sum:
        s = np.exp(rlogprobs).sum()
        # The resulting likelihood is exp(logfactor)*s
        return logfactor, s

    start_params = [2, 1, 2]
    # Let's try gradient descent
    return optim.minimize(target_neglikelihood, start_params, method='Nelder-Mead')


def inverse_scale(params):
    """Switch between scale and inverse_scale parametrization:
        return (params[0], params[1], 1/params[2])"""
    return (params[0], params[1], 1/params[2])


def gamma_negloglikelihood(params, x):
    """Do not use, there is `gamma.nnlf`"""
    # Note that this is a density.
    shape, loc, beta = params
    scale = 1./beta
    logprobas = gamma.logpdf(x, shape, loc, scale)
    notfinite = (~np.isfinite(logprobas)).sum()
    if notfinite:
        logger.warning('%d terms are not finite.', notfinite)
    return - (logprobas.sum())

def gamma_jac_negloglikelihood(params, x):
    """Jacobian (first derivative) of the gamma_negloglikelihood"""
def gamma_hess_negloglikelihood(params, x):
    """Jacobian (first derivative) of the gamma_negloglikelihood"""

def optim_gamma(x, **optim_kw):
    """See gamma.fit()"""
    start = optim_gamma_moments(x)
    return optim.minimize(gamma_negloglikelihood, start, x,
                          #bounds=[(0, None), (None,None), (0, None)],
                          **optim_kw)


def optim_gamma_moments(x):
    """Use the method of moments to find the gamma parameters"""
    # First translate x to start from 0:
    loc = x.min()
    x -= loc
    # E(X) = shape/beta = a/b
    # E(X²) = a(a+1)/b²
    # This should be simpler with Var(x) = a/b²
    mx = x.mean()
    vx = x.var(ddof=0)
    return mx*mx/vx, loc, vx/mx


# See scipy.misc.logsumexp
def small_sum_as_logfactors(logx):
    """When x is a list of very large negative numbers, factorize the sum of
    its exponentials.
    
    i.e., instead of computing exp(logx).sum() which risks underflow:
    
    logx = [-5000, -3000, -800]
    the first factor is -800

    So log(S) = -800*log(exp(-4200) + exp(-2200) + 1)

    and recursively: the next factor is -2200:
    
    log(S) = -800*log( -2200*log(exp(-2000) + 1) + 1)
    """

    logfactors = np.ones(len(logx)-1, dtype=np.longdouble)
    min_positions = np.ones(len(logx), dtype=int)

    tmp_logx = logx.copy()
    for i in range(len(logx)):
        logfactors[i] = tmp_logx.max()
        if logfactors[i] > 0:
            break
        min_positions[i] = tmp_logx.argmax()  # Need to determine if there are multiple max.
        tmp_logx -= logfactors[i]


