Analysis of duplication rates for the Ensembl forest
====================================================

(C) Guillaume Louvel 2020

Scripts used for chapter II of PhD thesis
"Datations dans les arbres de gènes : spéciations, duplications, pertes"

