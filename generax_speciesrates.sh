#!/usr/bin/env bash

set -euo pipefail
IFS=$'\t\n'

# Condor submission file:
#     ~/ws7/DUPLI_data93/alignments/generax_Simiiformes_speciesrates.condor.txt
# Run 120h on 30 cores.
# NOTE: Fix the transfer rate at zero.
#     ~/ws7/DUPLI_data93/alignments/generax_Simiiformes_speciesrates.condor.txt
# In addition: there was a duplicated 'Mus' node in the species tree used here.

# Output directory:
outdir=$HOME/ws7/DUPLI_data93/alignments/generax_speciesrates/
dtlfile=$outdir/gene_optimization_4/dtl_rates.txt

cd $outdir
awk '{print NF}' gene_optimization_4/dtl_rates.txt
## 429

# This counts the number of branches:
indent_nwk.py starting_species_tree.newick | grep -cv '^ *($'
# Or:
egrep -o '[A-Z][A-Za-z0-9_]+:[0-9]+\.[0-9]+' starting_species_tree.newick | wc -l
## 143

# Supposing the 143 D rates are consecutive, then L, then T.
Drates=($(sed 's/\s\+/\n/g' gene_optimization_4/dtl_rates.txt | head -143))
Lrates=($(sed 's/\s\+/\n/g' gene_optimization_4/dtl_rates.txt | sed -n '144,286p'))
Trates=($(sed 's/\s\+/\n/g' gene_optimization_4/dtl_rates.txt | tail -n 143))

# Now tabulate that into a file:
branch=0

#indent_nwk.py starting_species_tree.newick | grep -v '^ *($' | sed -r 's/^ *\)?|[,;]$//g; s/:/\t/;' \
egrep -o '[A-Z][A-Za-z0-9_]+:[0-9]+\.[0-9]+' starting_species_tree.newick \
    | sed 's/:/\t/' \
    | while IFS= read -r line; do
        echo -e "${line}\t${Drates[$branch]}\t${Lrates[$branch]}\t${Trates[$branch]}"
        ((branch++))
      done > branch_dtl_rates.tsv

# Plot this in Python: ./generax_speciesrates_plot.py

